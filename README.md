**Lambdas**

A Basic Lambda, let us start from some classic code which we have been using,

```
List<String> list = new ArrayList<>();
list.add("John");
list.add("Michael");
list.add("Robert");
list.add("SomeName");



Collections.sort(list, new Comparator<String>() {
    @Override
    public int compare(String o1, String o2) {
        return o2.compareTo(o1);
    }
});
```




You can safely remove new, name, method name,  types and return statement and this is what is lambda expression.

`Collections.sort(list, (o1,o2) -> o2.compareTo(o1));`

- It looks that the underlying implementation is similar to anonymous classes.
- Do we have .class file getting generated if implementation is similar to anonymous class ? No class is generated.


Let us take another example, remove some names based on filter criteria.

```
Test.java

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("John");
        list.add("Michael");
        list.add("Robert");
        list.add("SomeName");

        Predicate<String> predicate = item -> item.length()>4;
        System.out.println("Class Name :" + predicate.getClass());
        list.removeIf(predicate);
 }
}
```


`list.removeIf(item -> item.length()>4);`


What we can conclude from above expression,

- "item -> item.length()>4" is lambda expression
- looks like code as data
- short or concise code
- easy to understand (debatable)
- it harnesses multi core world of processors


**Couple of questions**

**1. Lambda looks like code but Java being statically typed language, what is the type of lambda?**

**2. If implementation is not similar to anonymous classes, then how lambda works ? It is said that it was a change that happened in decade in Java which made lambda implementation possible.**


Let us try to find answer for each,

**1. Lambda looks like code but Java being statically typed language, what is the type of lambda?**

    Functional Interfaces in Java, your lambda is instance of Functional Interface. 
       - Only one abstract method
       - Predefined Functional interfaces     

```
         Predicate<T>    : test: T􏰌 -> Boolean  //  list.removeIf(item -> item.length()>4);
	     Function<T,R>   : apply: T -> R
         Consumer<T>   : accept: T -> void
         Supplier<T>      : get: () -> T
         UnaryOperator<T> : apply: T -> T  // 2 -> 4
```


So, your lambda is instance of Functional Interface. You have to have equivalent functional interface corresponding to your lambda expression.

When we are saying that it is instance of functional interface, that means that has to be a class being created. But we did not see any class after compilation process.

Before moving ahead, lets discuss one more concept - **Capturing and Non Capturing lambda**.

```
int base = 1;
list.removeIf(item -> item.length()>4+base);
```


lambda is capturing some value “base” from the enclosing context. This “base” must be final or effectively final. Such lambda expression which capture some values from enclosing context are called as Capturing Lambdas.


**2. If implementation is not similar to anonymous classes, then how lambda works ? It is said that it was a change that happened in decade in Java which made lambda implementation possible.**

Let us try to run below piece of code,

```
Code:
Predicate<String> predicate = item -> item.length()>4;
System.out.println("Class Name :" + predicate.getClass());
list.removeIf(predicate);
```


```
Output:
Class Name :class Test$$Lambda$14/0x0000000800b67840
```

It gives you class name, so it means a class gets generated. Class generation happens by runtime and not during compilation. But during compilation, code is emitted/added which helps to generate class by runtime.

**What happens during compilation ?**

1. Lambda converted to a method which will be invoked through  invokedynamic instruction.
          
   ** item -> item.length()>4+base** is converted to a static method. Below is disassembled code from Test.class

```
$ javap -p Test.class 
Compiled from "Test.java"
public class Test {
  public Test();
  public static void main(java.lang.String[]);
  private static boolean lambda$main$0(int, java.lang.String);
}
```


3.  The bootstrap method information which is needed for JVM to construct object representation of lambda during runtime.

Use javap -v to get information about this bootstrap method

```
BootstrapMethods:
  0: #86 REF_invokeStatic java/lang/invoke/LambdaMetafactory.metafactory:(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodHandle;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;
    Method arguments:
      #93 (Ljava/lang/Object;)Z
      #94 REF_invokeStatic Test.lambda$main$0:(ILjava/lang/String;)Z
      #97 (Ljava/lang/String;)Z
  1: #99 REF_invokeStatic java/lang/invoke/StringConcatFactory.makeConcatWithConstants:(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/invoke/CallSite;
    Method arguments:
      #105 Class Name :\u0001
InnerClasses:
  public static final #112= #108 of #110; // Lookup=class java/lang/invoke/MethodHandles$Lookup of class java/lang/invoke/MethodHandles
```

**What happens during runtime?**

First time, when JVM executes an invoke dynamic instruction,
 - it consults bootstrap method (generated during compilation process) to prepare CallSite. A CallSite can be considered having information about Method Handle. So, bootstrap method returns a linked CallSite
- CallSite can embed conditions under which it may need relinking like arguments type changing etc. If there is no change in these conditions, JVM does not consult bootstrap again.

Bootstrap method — > CallSite —> Method Handle  —> lambda method

**How to capture class generated during runtime for lambda ?**

Set system property “jdk.internal.lambda.dumpProxyClasses” to some path and run your program.
Lambda class will be generated at this path. For example, here is the name of class generated,

Test$$Lambda$1.class

You can use javap command to see the class instructions, here is a snippet of code taken through verbose option.

```
javap -p Test\$\$Lambda\$1.class 
final class Test$$Lambda$1 implements java.util.function.Predicate {
  private final int arg$1;
  private Test$$Lambda$1(int);
  private static java.util.function.Predicate get$Lambda(int);
  public boolean test(java.lang.Object);
}
……..
………

{
  public boolean test(java.lang.Object);
    descriptor: (Ljava/lang/Object;)Z
    flags: (0x0001) ACC_PUBLIC
    Code:
      stack=2, locals=2, args_size=2
         0: aload_0
         1: getfield      #15                 // Field arg$1:I
         4: aload_1
         5: checkcast     #24                 // class java/lang/String
         8: invokestatic  #30                 // Method Test.lambda$main$0:(ILjava/lang/String;)Z
        11: ireturn
    RuntimeVisibleAnnotations:
      0: #22()
        jdk.internal.vm.annotation.Hidden
}
```

 




